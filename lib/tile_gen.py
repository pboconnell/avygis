from colour import Color
from numpy import pi
import numpy as np
import scipy.misc as smp
import itertools
import math
import json
from shapely.geometry import shape, Point
from avy_gis.lib import slippy_funcs as sf
from avy_gis.lib.topo_db import topo_db

db = topo_db()

r = Color('red')
p = Color('purple')
transparent = [0,0,0,0]
gradient = list(r.range_to(p, 91))

def getColorIndex(rad, minA, maxA):
	if(rad < minA):
		rad = minA
	if(rad > maxA):
		rad = maxA
	return int(round(((rad)/(maxA)) * 90))

def color_to_rgb(c):
	return map(lambda x:int(round(x * 255)), [c.get_red(),c.get_green(),c.get_blue(), 1])

def area_string_to_num(area):
	if area == 'mh':
		return 0
	elif area == 'cws':
		return 1
	elif area == 'ces':
		return 2
	elif area == 'cwc':
		return 3
	elif area == 'sqp':
		return 4
	elif area == 'svp':
		return 5
	elif area == 'cec':
		return 6
	elif area == 'cwn':
		return 7
	elif area == 'cen':
		return 8
	elif area == 'olm':
		return 9
	else:
		return -1

def area_num_to_string(area):
	if area == 0:
		return 'mh'
	elif area == 1:
		return 'cws'
	elif area == 2:
		return 'ces'
	elif area == 3:
		return 'cwc'
	elif area == 4:
		return 'sqp'
	elif area == 5:
		return 'svp'
	elif area == 6:
		return 'cec'
	elif area == 7:
		return 'cwn'
	elif area == 8:
		return 'cen'
	elif area == 9:
		return 'olm'
	else:
		return ''

def make_aspect_tile(x, y, z):
	minA = -1
	maxA = 6.28319

	start_y, start_x = sf.num2deg(x, y, z)
	width, height = sf.tile_dimensions(x,y,z)
	bbox = {}
	bbox['sw'] = {}
	bbox['ne'] = {}

	bbox['sw']['lat'] = start_y - height
	bbox['sw']['lon'] = start_x
	bbox['ne']['lat'] = start_y
	bbox['ne']['lon'] = start_x + width
	topo_data = db.get_bbox(bbox, ['aspect'])

	img_data = []
	for i in range(0,256):
		img_data.insert(0,[])
		for j in range(0,256):
			yi = int(round((len(topo_data['aspect'])-1) * i/256.0))
			xi = int(round((len(topo_data['aspect'][0])-1) * j/256.0))
			index = getColorIndex(topo_data['aspect'][yi][xi], minA, maxA)
			c = gradient[index]
			img_data[0].append(color_to_rgb(c))
	img = smp.toimage(img_data)
	return img
	# img.save("test_tile_aspect_"+str(x)+"_"+str(y)+"_"+str(z)+".png")

def make_topo_mask_tile(x, y, z):
	start_y, start_x = sf.num2deg(x, y, z)
	width, height = sf.tile_dimensions(x,y,z)

	bbox = {}
	bbox['sw'] = {}
	bbox['ne'] = {}

	bbox['sw']['lat'] = start_y - height
	bbox['sw']['lon'] = start_x
	bbox['ne']['lat'] = start_y
	bbox['ne']['lon'] = start_x + width
	topo_data = db.get_bbox(bbox, ['height', 'aspect', 'mag'])

	ntl_bottom = 1219
	ntl_top = 1524

	tile_data = []
	for i in range(0,256):
		tile_data.insert(0,[])
		for j in range(0,256):
			yi = int(round((len(topo_data['aspect'])-1) * i/256.0))
			xi = int(round((len(topo_data['aspect'][0])-1) * j/256.0))

			if(topo_data['mag'][yi][xi] < 0.47):
				tile_data[0].append(np.int8(-1))
			elif(topo_data['height'][yi][xi] >= ntl_top):
				if(topo_data['aspect'][yi][xi] >= 1.1781 and topo_data['aspect'][yi][xi] < 1.9635):
					tile_data[0].append(np.int8(0))
				elif(topo_data['aspect'][yi][xi] >= 1.9635 and topo_data['aspect'][yi][xi] < 2.7489):
					tile_data[0].append(np.int8(7))
				elif(topo_data['aspect'][yi][xi] >= 2.7489 and topo_data['aspect'][yi][xi] < 3.5343):
					tile_data[0].append(np.int8(6))
				elif(topo_data['aspect'][yi][xi] >= 3.5343 and topo_data['aspect'][yi][xi] < 4.3197):
					tile_data[0].append(np.int8(5))
				elif(topo_data['aspect'][yi][xi] >= 4.3197 and topo_data['aspect'][yi][xi] < 5.1051):
					tile_data[0].append(np.int8(4))
				elif(topo_data['aspect'][yi][xi] >= 5.1051 and topo_data['aspect'][yi][xi] < 5.8905):
					tile_data[0].append(np.int8(3))
				elif(topo_data['aspect'][yi][xi] >= 5.8905 or topo_data['aspect'][yi][xi] < 0.3927):
					tile_data[0].append(np.int8(2))
				elif(topo_data['aspect'][yi][xi] >= 0.3927 and topo_data['aspect'][yi][xi] < 1.1781):
					tile_data[0].append(np.int8(1))
			elif(topo_data['height'][yi][xi] < ntl_top and topo_data['height'][yi][xi] >= ntl_bottom):
				if(topo_data['aspect'][yi][xi] >= 1.1781 and topo_data['aspect'][yi][xi] < 1.9635):
					tile_data[0].append(np.int8(8))
				elif(topo_data['aspect'][yi][xi] >= 1.9635 and topo_data['aspect'][yi][xi] < 2.7489):
					tile_data[0].append(np.int8(15))
				elif(topo_data['aspect'][yi][xi] >= 2.7489 and topo_data['aspect'][yi][xi] < 3.5343):
					tile_data[0].append(np.int8(14))
				elif(topo_data['aspect'][yi][xi] >= 3.5343 and topo_data['aspect'][yi][xi] < 4.3197):
					tile_data[0].append(np.int8(13))
				elif(topo_data['aspect'][yi][xi] >= 4.3197 and topo_data['aspect'][yi][xi] < 5.1051):
					tile_data[0].append(np.int8(12))
				elif(topo_data['aspect'][yi][xi] >= 5.1051 and topo_data['aspect'][yi][xi] < 5.8905):
					tile_data[0].append(np.int8(11))
				elif(topo_data['aspect'][yi][xi] >= 5.8905 or topo_data['aspect'][yi][xi] < 0.3927):
					tile_data[0].append(np.int8(10))
				elif(topo_data['aspect'][yi][xi] >= 0.3927 and topo_data['aspect'][yi][xi] < 1.1781):
					tile_data[0].append(np.int8(9))
			elif(topo_data['height'][yi][xi] < ntl_bottom):
				if(topo_data['aspect'][yi][xi] >= 1.1781 and topo_data['aspect'][yi][xi] < 1.9635):
					tile_data[0].append(np.int8(16))
				elif(topo_data['aspect'][yi][xi] >= 1.9635 and topo_data['aspect'][yi][xi] < 2.7489):
					tile_data[0].append(np.int8(23))
				elif(topo_data['aspect'][yi][xi] >= 2.7489 and topo_data['aspect'][yi][xi] < 3.5343):
					tile_data[0].append(np.int8(22))
				elif(topo_data['aspect'][yi][xi] >= 3.5343 and topo_data['aspect'][yi][xi] < 4.3197):
					tile_data[0].append(np.int8(21))
				elif(topo_data['aspect'][yi][xi] >= 4.3197 and topo_data['aspect'][yi][xi] < 5.1051):
					tile_data[0].append(np.int8(20))
				elif(topo_data['aspect'][yi][xi] >= 5.1051 and topo_data['aspect'][yi][xi] < 5.8905):
					tile_data[0].append(np.int8(19))
				elif(topo_data['aspect'][yi][xi] >= 5.8905 or topo_data['aspect'][yi][xi] < 0.3927):
					tile_data[0].append(np.int8(18))
				elif(topo_data['aspect'][yi][xi] >= 0.3927 and topo_data['aspect'][yi][xi] < 1.1781):
					tile_data[0].append(np.int8(17))
	
	return tile_data

def make_area_mask_tile(x, y, z, areas):
	start_y, start_x = sf.num2deg(x, y, z)
	width, height = sf.tile_dimensions(x,y,z)

	bbox = {}
	bbox['sw'] = {}
	bbox['ne'] = {}

	bbox['sw']['lat'] = start_y - height
	bbox['sw']['lon'] = start_x
	bbox['ne']['lat'] = start_y
	bbox['ne']['lon'] = start_x + width

	tile_data = []
	for i in range(0,256):
		tile_data.insert(0,[])
		for j in range(0,256):
			curr_lon = start_x + width * (i/256.0)
			curr_lat = start_y - height * (j/256.0)
			point = Point(curr_lon, curr_lat)
			area_id = np.int8(-1)
			for key in areas:
				poly = shape(areas[key])
				if(poly.contains(point)):
					area_id = np.int8(area_string_to_num(key))
					break
			tile_data[0].append(area_id)
		
	return tile_data

def make_avy_tile(x, y, z, avy_dict):
	topo_mask = db.get_topo_mask_tile(x,y,z)
	area_mask = db.get_area_mask_tile(x,y,z)

	for area in avy_dict:
		print(area)
		avy_dict[area].sort(key=lambda x: x['level'])

	img_data = np.full((256,256,4), (transparent))

	for i in range(0,256):
		for j in range(0,256):
			area_key = area_num_to_string(area_mask[i][j])
			if not avy_dict.has_key(area_key):
				continue
			for hazard in avy_dict[area_key]:
				if(topo_mask[i][j] in hazard['affected_zones']):
					if(hazard['level'] is 1):
						c = Color('green')
					elif(hazard['level'] is 2):
						c = Color('yellow')
					elif(hazard['level'] is 3):
						c = Color('orange')
					elif(hazard['level'] is 4):
						c = Color('red')
					elif(hazard['level'] is 5):
						c = Color('purple')
					img_data[i][j] = color_to_rgb(c)

	img = smp.toimage(img_data)
	# img.save("test_avy_tile_"+str(x)+"_"+str(y)+"_"+str(z)+".png")
	return img
