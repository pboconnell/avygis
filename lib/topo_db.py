import mysql.connector
import json
import math
import os
import numpy as np
import avy_gis.dbconfig as dbconfig

class topo_db:

	def __init__(self, tile_width=3600):
		self.dir_path = os.path.dirname(os.path.realpath(__file__))
		self.tile_width = tile_width

		try:
			self.mydb = mysql.connector.connect(
				host=dbconfig.host,
				user=dbconfig.user,
				passwd=dbconfig.password,
				database=dbconfig.database
			)
			self.cursor = self.mydb.cursor()
		except Exception as err:
			print('issue initializing db:')
			print(err)
			raise


	def check_bounds(self, lat, lon):
		lati, loni = self._coord_to_index(lat, lon)
		sql_statement = 'select count(id) from location_data where lat={} and lon={}'.format(lati, loni)
		self.cursor.execute(sql_statement)
		sql_results = self.cursor.fetchone()
		return False if sql_results[0] is 0 else True

	def get_point(self, lat, lon, fields):
		assert(self.check_bounds(lat, lon)), 'coordinate out of bounds'

		fields_str = ', '.join(fields)
		lati, loni = self._coord_to_index(lat, lon)
		print('{}, {}'.format(lati, loni))
		sql_statement = 'select {} from location_data where lat={} and lon={}'.format(fields_str, lati, loni)
		print(sql_statement)

		try:
			self.cursor.execute(sql_statement)
			sql_results = self.cursor.fetchone()
			if(not sql_results):
				return None
			res = {}
			for i in range(0,len(fields)):
				res[fields[i]] = sql_results[i]
			return res
		except mysql.connector.Error as err:
			print('mysql error:')
			print(err)
			raise


	def get_bbox(self, bbox, fields):
		assert(self.check_bounds(bbox['sw']['lat'], bbox['sw']['lon']) and self.check_bounds(bbox['sw']['lat'], bbox['ne']['lon']) and self.check_bounds(bbox['ne']['lat'], bbox['sw']['lon']) and self.check_bounds(bbox['ne']['lat'], bbox['ne']['lon'])), 'bbox out of bounds {},{} {},{}'.format(bbox['sw']['lon'], bbox['sw']['lat'], bbox['ne']['lon'], bbox['ne']['lat'])
		
		fields_str = ', '.join(fields)
		latmin, lonmin = self._coord_to_index(bbox['sw']['lat'], bbox['sw']['lon'])
		latmax, lonmax = self._coord_to_index(bbox['ne']['lat'], bbox['ne']['lon'])
		width = lonmax - lonmin + 1
		sql_statement = 'select {} from location_data where lat between {} and {} and (lon between {} and {})'.format(fields_str, latmin, latmax, lonmin, lonmax)
		self.cursor.execute(sql_statement)
		sql_results = self.cursor.fetchall()
		res = {}
		for i in range(0,len(fields)):
			res[fields[i]] = [x[i] for x in sql_results]
			try:
				res[fields[i]] = np.reshape(res[fields[i]], (len((res[fields[i]]))/width,width))
			except Exception as e:
				print('missing data edge case')
				print(e)
				raise
		return res

	def get_topo_mask_tile(self, x, y, z):
		sql_statement = 'select data from mask_tiles where type=0 and z={} and x={} and y={}'.format(z,x,y)
		self.cursor.execute(sql_statement)
		sql_results = self.cursor.fetchone()
		assert(sql_results), 'mask tile not found.'

		print(len(sql_results[0]))
		res_list = list(sql_results[0])
		print(len(res_list))
		res_list = map(lambda x: np.int8(x), res_list)
		print(len(res_list))
		res_list = np.reshape(res_list, (256, 256))
		return res_list
	
	def get_area_mask_tile(self, x, y, z):
		sql_statement = 'select data from mask_tiles where type=1 and z={} and x={} and y={}'.format(z,x,y)
		self.cursor.execute(sql_statement)
		sql_results = self.cursor.fetchone()
		assert(sql_results), 'area tile not found.'

		print(len(sql_results[0]))
		res_list = list(sql_results[0])
		print(len(res_list))
		res_list = map(lambda x: np.int8(x), res_list)
		print(len(res_list))
		res_list = np.reshape(res_list, (256, 256))
		return res_list

	def _coord_to_index(self, lat, lon):
		start_index_lon = int(math.floor(lon) * self.tile_width)
		offset_lon = int(round((lon - math.floor(lon)) * self.tile_width))
		loni = start_index_lon + offset_lon

		start_index_lat = int(math.floor(lat) * self.tile_width)
		offset_lat = int(round((1 - (lat - math.floor(lat))) * self.tile_width))
		lati = start_index_lat - offset_lat
		return (lati,loni)