import mysql.connector
import avy_gis.dbconfig as dbconfig


mydb = mysql.connector.connect(
	host=dbconfig.host,
	user=dbconfig.user,
	passwd=dbconfig.password,
	database=dbconfig.database
)
cursor = mydb.cursor()

dir_path = os.path.dirname(os.path.realpath(__file__))