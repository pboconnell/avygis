import mysql.connector
from avy_gis.lib import tile_gen as tg
import avy_gis.dbconfig as dbconfig
import io
import json
import os

dir_path = os.path.dirname(os.path.realpath(__file__))
with open(os.path.join(dir_path,'area_avy_dictionary.json')) as f:
	avy_dict = json.load(f)

mydb = mysql.connector.connect(
	host=dbconfig.host,
	user=dbconfig.user,
	passwd=dbconfig.password,
	database=dbconfig.database
)
cursor = mydb.cursor()

tile = tg.make_avy_tile(1326, 2886, 13, avy_dict)

png_bytes = 0
with io.BytesIO() as output:
	tile.save(output, format="PNG")
	png_bytes = output.getvalue()
qs = "insert ignore into tiles (x, y, z, type, data) values (%s, %s, %s, %s, %s);"
# print("size of tile: " + str(len(png_bytes)))
args = (1326,2886,13,0,png_bytes)

cursor.execute(qs,args)
mydb.commit()
