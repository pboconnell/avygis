import json
import mysql.connector
from avy_gis.lib import tile_gen
import scipy.misc as smp
import os
import array
import struct
import io
from avy_gis.lib import slippy_funcs as sf
from shapely.geometry import shape, Point
import avy_gis.dbconfig as dbconfig

mydb = mysql.connector.connect(
	host=dbconfig.host,
	user=dbconfig.user,
	passwd=dbconfig.password,
	database=dbconfig.database
)
cursor = mydb.cursor()

dir_path = os.path.dirname(os.path.realpath(__file__))

with open(os.path.join(dir_path,'area_geometry_dictionary.json')) as f:
	geo_dict = json.load(f)

with open(os.path.join(dir_path,'area_avy_dictionary.json')) as f:
	avy_dict = json.load(f)

cwc_poly = shape(geo_dict['cws'])
print(cwc_poly.bounds)
minx, miny = sf.deg2num(cwc_poly.bounds[3], cwc_poly.bounds[0], 13)
maxx, maxy = sf.deg2num(cwc_poly.bounds[1], cwc_poly.bounds[2], 13)
print(minx, miny)
print(maxx, maxy)

for x in range(minx, maxx):
	for y in range(miny, maxy):
		# try:
		# 	topo_mask_data = tile_gen.make_topo_mask_tile(x, y, 13)
		# except:
		# 	print('skipping tile {}, {}'.format(x,y))
		# 	continue
		# topo_bytes = b''

		# for row in topo_mask_data:
		# 	for col in row:
		# 		topo_bytes = topo_bytes + struct.pack('b', col)

		# print('topo_bytes: {}'.format(len(topo_bytes)))

		# qs = "insert ignore into mask_tiles (x, y, z, type, data) values (%s, %s, %s, %s, %s);"
		# args = (x,y,13,0,topo_bytes)
		# cursor.execute(qs,args)
		# mydb.commit()

		# area_mask_data = tile_gen.make_area_mask_tile(x, y, 13, geo_dict)
		# area_bytes = b''

		# for row in area_mask_data:
		# 	for col in row:
		# 		area_bytes = area_bytes + struct.pack('b', col)

		# print('area_bytes: {}'.format(len(area_bytes)))

		# qs = "insert ignore into mask_tiles (x, y, z, type, data) values (%s, %s, %s, %s, %s);"
		# args = (x,y,13,1,area_bytes)
		# cursor.execute(qs,args)
		# mydb.commit()




		try:
			tile = tile_gen.make_avy_tile(x, y, 13, avy_dict)
		except:
			print('skipping avy tile: {}, {}'.format(x,y))
			continue
		png_bytes = 0
		with io.BytesIO() as output:
			tile.save(output, format="PNG")
			png_bytes = output.getvalue()
		qs = "replace into tiles (x, y, z, type, data) values (%s, %s, %s, %s, %s);"
		# print("size of tile: " + str(len(png_bytes)))
		args = (x,y,13,0,png_bytes)
		
		cursor.execute(qs,args)
		mydb.commit()





# CREATE TABLE `mask_tiles` (
#   `x` int(11) DEFAULT NULL,
#   `y` int(11) DEFAULT NULL,
#   `z` int(11) DEFAULT NULL,
#   `type` int(11) DEFAULT NULL,
#   `id` int(11) NOT NULL AUTO_INCREMENT,
#   `data` blob,
#   PRIMARY KEY (`id`),
#   UNIQUE KEY `type_z_x_y_unique` (`type`,`z`,`x`,`y`)
# ) ENGINE=InnoDB DEFAULT CHARSET=latin1