import base64

fn = "N46W122.hgt"
fns = "N46W122.slp"
res = 3601 # resolution of hgt file
bytes_per_row = 2 * res

# with open(fn, 'rb') as f:
# 	try:
# 		b = f.read(2)
# 		maxelv = b
# 		minelv = b
# 		while b != b"":
# 			if b > maxelv:
# 				maxelv = b
# 			elif b < minelv:
# 				minelv = b
# 			b = f.read(2)
# 	finally:
# 		f.close()
# 	print(maxelv.hex())
# 	print(minelv.hex())

# Get elevation in 16 bit signed int meters
def getElevation(lat, lon):
	rowoff = int(round((1 - (lat % 1)) * res)) # Number of rows to offset
	coloff = int(round(((lon % 1)) * res)) * 2 # Number of bytes to offset in last row
	offset = bytes_per_row * rowoff + coloff # Total offset
	with open(fn, 'rb') as f:
		try:
			f.seek(offset)
			elevation = f.read(2)
			# print(elevation.hex())
		finally:
			f.close()

def bytes_to_int(bytes):
	result = 0
	for b in bytes:
			result = result * 256 + int(b)
	return result

def base64GeoSlope(north, south, east, west):
	data = {}
	if(north >= 47 or south < 46 or east > -121 or west < -122) :
		data['ptsPerDegree'] = 0
		data['ptsPerRow'] = 0
		data['base64String'] = "bad"
		data['sw'] = {}
		data['sw']['lat'] = 0
		data['sw']['lon'] = 0
		data['min'] = 0
		data['diff'] = 0
		return data
	rowoff = int(round((1 - (north % 1)) * res)) # Number of rows to offset
	coloff = int(round(((west % 1)) * res)) # Number of bytes to offset in last row
	start = 2 * res * rowoff + coloff # Initial offset in file
	end = int(round(((east % 1)) * res)) # End each row on [end]th byte

	rows = int(round((north - south) * res)) # Number of rows in bounding box
	cols = int(round((east - west) * res)) # Number of cols in bounding box
	count = rows * cols # Total number of entries to read



	pointStr = b'' # bytestring of points
	with open(fns, 'rb') as f:
		try:
			f.seek(start)
			el = f.read(1)
			count -= 1
			maxelv = el
			minelv = el
			while(count > 0):
				count -= 1
				el = f.read(1)
				if el > maxelv:
					maxelv = el
				elif el < minelv:
					minelv = el
				pointStr += el
				if(f.tell() % res == end):
					f.seek((res - end) + coloff, 1) # Seek to start of next row
		finally:
			f.close()
	b64 = base64.b64encode(pointStr) # Convert bytestring to base64

	b64Str = str(b64)[2:-1]
	data['ptsPerDegree'] = res
	data['ptsPerRow'] = cols
	data['base64String'] = b64Str
	data['sw'] = {}
	data['sw']['lat'] = south
	data['sw']['lon'] = west
	data['min'] = bytes_to_int(minelv)
	data['diff'] = bytes_to_int(maxelv) - bytes_to_int(minelv)
	# base64Geo = "{'ptsPerDegree': "+str(ptsPerDegree)+",'ptsPerRow': "+str(ptsPerRow)+",'base64String':'"+str(b64Str)+"','sw':{'lat': "+str(south)+",'lon': "+str(west)+"} }"
	return data



# Get base64Geo string for rectangle bound by north, south, east, west
def base64GeoElevation(north, south, east, west):
	data = {}
	if(north >= 47 or south < 46 or east > -121 or west < -122) :
		data['ptsPerDegree'] = 0
		data['ptsPerRow'] = 0
		data['base64String'] = "bad"
		data['sw'] = {}
		data['sw']['lat'] = 0
		data['sw']['lon'] = 0
		data['min'] = 0
		data['diff'] = 0
		return data
	rowoff = int(round((1 - (north % 1)) * res)) # Number of rows to offset
	coloff = int(round(((west % 1)) * res)) * 2 # Number of bytes to offset in last row
	start = 2 * res * rowoff + coloff # Initial offset in file
	end = int(round(((east % 1)) * res)) * 2 # End each row on [end]th byte

	rows = int(round((north - south) * res)) # Number of rows in bounding box
	cols = int(round((east - west) * res)) # Number of cols in bounding box
	count = rows * cols # Total number of entries to read



	pointStr = b'' # bytestring of points
	with open(fn, 'rb') as f:
		try:
			f.seek(start)
			el = f.read(2)
			count -= 1
			maxelv = el
			minelv = el
			while(count > 0):
				count -= 1
				el = f.read(2)
				if el > maxelv:
					maxelv = el
				elif el < minelv:
					minelv = el
				pointStr += el
				if(f.tell() % bytes_per_row == end):
					f.seek((bytes_per_row - end) + coloff, 1) # Seek to start of next row
		finally:
			f.close()
	b64 = base64.b64encode(pointStr) # Convert bytestring to base64

	b64Str = str(b64)[2:-1]
	data['ptsPerDegree'] = res
	data['ptsPerRow'] = cols
	data['base64String'] = b64Str
	data['sw'] = {}
	data['sw']['lat'] = south
	data['sw']['lon'] = west
	data['min'] = bytes_to_int(minelv)
	data['diff'] = bytes_to_int(maxelv) - bytes_to_int(minelv)
	# base64Geo = "{'ptsPerDegree': "+str(ptsPerDegree)+",'ptsPerRow': "+str(ptsPerRow)+",'base64String':'"+str(b64Str)+"','sw':{'lat': "+str(south)+",'lon': "+str(west)+"} }"
	return data

# print(base64GeoElevation(46.853000, 46.852000, -121.762000, -121.763000))
# getElevation(46.851965, -121.761295)