from pydem.dem_processing import DEMProcessor
from colour import Color
from numpy import pi
import scipy.misc as smp
import itertools

def get_rainier(list):
	return map(lambda x: x[550:1291], list[223:824])

def rad_to_deg(rad):
	return rad * 180 / pi

def apply_slope_shading(rad, color):
	deg = rad_to_deg(rad)
	if(deg > 90):
		deg = 90
	if(deg < 0):
		deg = 0
	lum_ratio = 1 + (deg/90)/10000
	# print(color.luminance)
	color.luminance = color.luminance / lum_ratio
	# print(str(deg) + " : " + str(color.luminance) + " : " + str(lum_ratio))
	return color

# def getColorIndex(rad):
# 	deg = rad_to_deg(rad)
# 	if(deg > 90):
# 		return 90
# 	return int(round(deg))

def getColorIndex(rad, min, max):
	if(rad < 0):
		rad = 0
	return int(round(((rad)/(max)) * 90))
	# return int(round(((rad - min)/(max - min)) * 90))

def getColor(rad):
	deg = rad_to_deg(rad)
	if(deg < 35):
		return Color('green')
	elif(deg >= 35 and deg < 55):
		return Color('orange')
	else:
		return Color('red')

tiff_fn = './mergehgt/N46W122.tif'
dem_proc = DEMProcessor(tiff_fn)
mag, aspect = dem_proc.calc_slopes_directions()
heights = dem_proc.data.data

# mag = dem_proc.data.data[2777:3378][550:1291]
# mag = map(lambda x: x[550:1291], dem_proc.data.data[223:824])

min = aspect[0][0]
max = aspect[0][0]
for row in aspect:
	for col in row:
		if(col < min):
			min = col
		elif(col > max):
			max = col

print(max)
print(min)

# print("height: {}".format(heights[3118][844]))
# print("mag: {}".format(mag[3118][844]))
# print("aspect: {}".format(aspect[3118][844]))

print(len(mag))
print(len(mag[0]))

r = Color('red')
p = Color('purple')
gradient = list(r.range_to(p, 91))
print(len(gradient))
# gradient.extend(list(p.range_to(r, 91)))
# print(len(gradient))


img_data = []
i=0

for row_m, row_a, row_h in itertools.izip(mag, aspect, heights):
	img_data.append([])
	for col_m, col_a, col_h in itertools.izip(row_m, row_a, row_h):
		# print(col_a)
		index = getColorIndex(col_a, min, max)
		c = gradient[index]
		# c = apply_slope_shading(col_m, c)
		img_data[i].append(map(lambda x:int(round(x * 255)), [c.get_red(),c.get_green(),c.get_blue()]))
	i = i + 1

print(len(img_data))
print(len(img_data[0]))

img = smp.toimage(img_data)
print(dir(img))
print(img.getbbox())

img.save("aspect_test.png", "png")

# with open('image_rainbow_small.png', 'wb') as f:
# 	f.write(img._repr_png_())


# import numpy as np
# import scipy.misc as smp

# # Create a 1024x1024x3 array of 8 bit unsigned integers
# data = np.zeros( (1024,1024,3), dtype=np.uint8 )

# data[512,512] = [254,0,0]       # Makes the middle pixel red
# data[512,513] = [0,0,255]       # Makes the next pixel blue

# img = smp.toimage( data )       # Create a PIL image

# with open('img.png', 'wb') as f:
# 	f.write(img._repr_png_())