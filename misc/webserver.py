import elevationDB
from flask import Flask
from flask import jsonify
from flask import request
from flask import render_template
from flask import url_for
app = Flask(__name__)

@app.route('/getMap')
def getMap():
	return render_template('map.html')

@app.route('/getRect')
def getRect():
	bbox = str(request.args.get('bbox')).split(',')
	north = float(bbox[3])
	south = float(bbox[1])
	east = float(bbox[2])
	west = float(bbox[0])
	print(north,south,east,west)
	resp = elevationDB.base64GeoElevation(north,south,east,west)
	return jsonify(resp)

@app.route('/getSlope')
def getSlope():
	bbox = str(request.args.get('bbox')).split(',')
	north = float(bbox[3])
	south = float(bbox[1])
	east = float(bbox[2])
	west = float(bbox[0])
	print(north,south,east,west)
	resp = elevationDB.base64GeoSlope(north,south,east,west)
	return jsonify(resp)


if(__name__ == "__main__"):
	app.run()