from ....lib import tile_gen
from ....lib import topo_db
import mysql.connector
import json
from ....lib import slippy_funcs as sf
import io

# with open("./dbconfig.json") as f:
# 	db_config = json.load(f)

mydb = mysql.connector.connect(
	host=dbconfig['host'],
	user=dbconfig['user'],
	passwd=dbconfig['password'],
	database=dbconfig['database']
)

cursor = mydb.cursor()

bounds = []
bounds.append({'sw':{'lat':46,'lon':-122}, 'ne':{'lat':47, 'lon':-121}})
bounds.append({'sw':{'lat':47,'lon':-121}, 'ne':{'lat':48, 'lon':-120}})
bounds.append({'sw':{'lat':47,'lon':-122}, 'ne':{'lat':48, 'lon':-121}})
bounds.append({'sw':{'lat':48,'lon':-121}, 'ne':{'lat':49, 'lon':-120}})
bounds.append({'sw':{'lat':48,'lon':-122}, 'ne':{'lat':49, 'lon':-121}})

for zoom in range(11,15):
	for box in bounds:
		minx, miny = sf.deg2num(box['ne']['lat'], box['sw']['lon'], zoom)
		maxx, maxy = sf.deg2num(box['sw']['lat'], box['ne']['lon'], zoom)

		for i in range(minx, maxx + 1):
			for j in range(miny, maxy + 1):
				print('making tile: ' + str(i) + ', ' + str(j) + ', ' + str(zoom))
				try:
					tile = tile_gen.make_aspect_tile(i,j,zoom)
				except Exception as e:
					print("failed tile_gen: " + str(e))
					continue

				png_bytes = 0
				with io.BytesIO() as output:
					tile.save(output, format="PNG")
					png_bytes = output.getvalue()
				qs = "insert into tiles (x, y, z, type, data) values (%s, %s, %s, %s, %s);"
				# print("size of tile: " + str(len(png_bytes)))
				args = (i,j,zoom,1,png_bytes)
				
				cursor.execute(qs,args)
				mydb.commit()