#!/bin/bash
for file in out/*; do
	filename=$(echo "$file" | cut -f 1 -d '.')
	echo $filename
	gdal_translate $filename.hgt $filename.tif
done