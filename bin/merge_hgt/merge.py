import os
import sys
import errno
import re
import argparse

#https://dds.cr.usgs.gov/srtm/version1/United_States_1arcsec/1arcsec/N47W121.hgt.zip
#https://dds.cr.usgs.gov/srtm/version2_1/SRTM1/Region_01/N48W122.hgt.zip
#https://cloud.sdsc.edu/v1/AUTH_opentopography/Raster/SRTM_GL1/SRTM_GL1_srtm/North/North_30_60/N48W122.hgt

def main():

	dir_path = os.path.dirname(os.path.realpath(__file__))
	out_dir = dir_path

	parser = argparse.ArgumentParser()
	parser.add_argument('-i', nargs=2, required=True, help='2 input directories to merge')
	parser.add_argument('-o', nargs=1, help='relative path to output directory')
	p = parser.parse_args(sys.argv[1:])

	if(p.o):
		out_dir = os.path.join(dir_path,p.o[0])

	in_dir1 = os.path.join(dir_path, p.i[0])
	in_dir2 = os.path.join(dir_path, p.i[1])

	assert(os.path.isdir(in_dir1) and os.path.isdir(in_dir2)), 'invalid input directory'
	for filename in os.listdir(in_dir1):

		if(not re.match(r'.*hgt$', filename)):
			print('skipping ' + filename)
			continue

		fn1 = os.path.join(in_dir1, filename)
		fn2 = os.path.join(in_dir2, filename)
		fn_out = os.path.join(out_dir, filename)

		out = b''

		try:
			with open(fn1, 'rb') as f1:
				with open(fn2, 'rb') as f2:
					h1 = f1.read(2)
					h2 = f2.read(2)
					while h1 != b'':
						o = min(h1, h2)
						out = out + o
						h1 = f1.read(2)
						h2 = f2.read(2)
		except OSError as err:
			print(err)
			raise

		if not os.path.exists(os.path.dirname(fn_out)):
			try:
				os.makedirs(os.path.dirname(fn_out))
			except OSError as err:
				if err.errno != errno.EEXIST:
					raise

		try:
			with open(fn_out, 'wb') as fout:
				fout.write(out)
		except OSError as err:
			print(err)
			raise

if __name__ == '__main__':
	main()