from pydem.dem_processing import DEMProcessor
import mysql.connector
import json
import threading
import os
import sys
import re
import argparse
import dbconfig as dbconfig



def processTif(path, filename, width, db):


	_active_threads = []

	cursor = db.cursor()

	db_re = re.match(r'^(N|S)(\d+)(E|W)(\d+)\.TIF$',filename.upper())

	if(not db_re):
		return
	lat_sign = 1 if db_re.group(1) == 'N' else -1
	lon_sign = 1 if db_re.group(3) == 'E' else -1
	start_lat = lat_sign * int(db_re.group(2))
	start_lon = lon_sign * int(db_re.group(4))

	try:

		dem_proc = DEMProcessor(os.path.join(path,filename))
		mag, aspect = dem_proc.calc_slopes_directions()
		heights = dem_proc.data.data

		assert(len(heights) >= width), 'tile size mismatch {} {}'.format(len(heights), width)

		for i in range(0, width):
			db.commit()
			print('{} rows created.'.format(i*width))
			for j in range(0, width):
				lati = start_lat * width - i
				loni = start_lon * width + j
				# x = threading.Thread(target=addPoint, args=(cursor, lati, loni, mag[i][j], heights[i][j], aspect[i][j]))
				# _active_threads.append(x)				
				# x.start()
				addPoint(cursor, lati, loni, mag[i][j], heights[i][j], aspect[i][j])
				db.commit()
				
		# for thread in _active_threads:
		# 	thread.join()
		# 	db.commit()
	except Exception as e:
		print('failed processing tif')
		print(e)

def addPoint(db_cursor, lati, loni, mag, height, aspect):
	try:
		db_cursor.execute("replace into location_data (coord, mag, height, aspect) values (PointFromText('POINT({} {})'), {}, {}, {})".format(lati, loni, mag, height, aspect))
	except mysql.connector.Error as err:
		print('mysql error:')
		print(err)
		raise

def main():

	TILE_WIDTH = 3600

	dir_path = os.path.dirname(os.path.realpath(__file__))
	data_path = os.path.join(dir_path, 'data')

	parser = argparse.ArgumentParser()
	parser.add_argument('-i', nargs=1, help='relative path to data folder')
	parser.add_argument('-w', nargs=1, type=int, help='width of each data tile (default {})'.format(TILE_WIDTH))
	p = parser.parse_args(sys.argv[1:])

	active_threads = []


	if(p.i):
		data_path = os.path.join(dir_path, p.i[0])
		assert(os.path.isdir(data_path)), 'input folder must be a directory'
	if(p.w):
		TILE_WIDTH = p.w[0]
		assert(TILE_WIDTH > 0), 'tile width must be positive'

	try:
		mydb = mysql.connector.connect(
			host=dbconfig.host,
			user=dbconfig.user,
			passwd=dbconfig.password,
			database=dbconfig.database
		)
		cursor = mydb.cursor()

		sql_init = "CREATE TABLE IF NOT EXISTS `location_data` ( `id` int(11) NOT NULL AUTO_INCREMENT, `coord` POINT DEFAULT NULL, `mag` float DEFAULT NULL, `height` float DEFAULT NULL, `aspect` float DEFAULT NULL, PRIMARY KEY (`id`) ) ENGINE=InnoDB DEFAULT CHARSET=latin1"
		cursor.execute(sql_init)
		mydb.commit()
	except mysql.connector.Error as err:
		print('issue opening db connection:')
		print(err)
		raise

	print(os.listdir(data_path))
	total_entries = TILE_WIDTH * TILE_WIDTH * len(list(filter(lambda x: re.match(r'^(N|S)(\d+)(E|W)(\d+)\.TIF$',x.upper()), os.listdir(data_path))))
	print('{} rows to be created'.format(total_entries))

	for filename in os.listdir(data_path):
		print(filename)
		processTif(data_path,filename,TILE_WIDTH,mydb)
		# x = threading.Thread(target=processTif, args=(data_path,filename))
		# active_threads.append(x)
		# x.start()

if __name__ == '__main__':
	main()